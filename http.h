#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

#include "config.h"

WiFiClient wifiClient;
HTTPClient httpClient;

String getRequest(String url)
{
	String response = "";

	httpClient.begin(wifiClient, url);

	int code = httpClient.GET();
	Serial.println("Code: " + (String)code);

	if (code > 0)
	{
		response = httpClient.getString();
	}

	httpClient.end();

	return response;
}

void sendUpdate(float *values, int size)
{
	String url = TS_UPDATE_ADDR + "?api_key=" + TS_API_WRITE_KEY;

	for (int i = 0; i < size; i++)
	{
		url = url + "&field" + (String)(i + 1) + "=" + (String)values[i];
	}

	Serial.println("Making request to: " + url);

	String response = getRequest(url);

	Serial.println("Response:");
	Serial.println(response);
}