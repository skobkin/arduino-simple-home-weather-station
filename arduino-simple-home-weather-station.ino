#include <ESP8266WiFi.h>
#include <OneWire.h>
#include "SSD1306Wire.h"
#include "OLEDDisplayUi.h"
// Custom images
//#include "images.h"
#include <Ticker.h>
#include <uptime.h>

// Simple DS1820 wrapper
#include "temp.hpp"
// HTTP requests
#include "http.h"

// Initialize the OLED display using Wire library
SSD1306Wire display(0x3c, D1, D2);
OLEDDisplayUi ui(&display);

OneWire thermal1(D3);
OneWire thermal2(D4);

float t1Last = DS_TEMP_NULL;
float t2Last = DS_TEMP_NULL;

Ticker sendUpdateTicker;

bool itsTimeToSendUpdates = false;
long updatesSent = 0;

void updatesOverlay(OLEDDisplay *display, OLEDDisplayUiState *state)
{
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_10);

  display->drawString(0, 0, "Sent: " + (String)updatesSent);
}

void ipOverlay(OLEDDisplay *display, OLEDDisplayUiState *state)
{
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_10);
  display->drawString(128, 0, WiFi.localIP().toString());
}

void uptimeOverlay(OLEDDisplay *display, OLEDDisplayUiState *state)
{
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_10);

  String uptime = (String)uptime::getDays() + "D " + (String)uptime::getHours() + "H";

  display->drawString(0, 54, uptime);
}

void freqOverlay(OLEDDisplay *display, OLEDDisplayUiState *state)
{
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_10);

  display->drawString(128, 54, (String)ESP.getCpuFreqMHz() + "MHz");
}

void drawFrame1(OLEDDisplay *display, OLEDDisplayUiState *state, int16_t x, int16_t y)
{
  String t1Str, t2Str;

  if (DS_TEMP_NULL != t1Last)
  {
    t1Str = (String)t1Last + "C";
  }
  else
  {
    t1Str = "N/A";
  }

  if (DS_TEMP_NULL != t2Last)
  {
    t2Str = (String)t2Last + "C";
  }
  else
  {
    t2Str = "N/A";
  }

  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(0 + x, 10 + y, "Outdoor: " + t1Str);
  display->drawString(0 + x, 26 + y, "Room:    " + t2Str);
}

void drawFrame2(OLEDDisplay *display, OLEDDisplayUiState *state, int16_t x, int16_t y)
{
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(0 + x, 10 + y, "reserved");
}

// UI frames
FrameCallback frames[] = {drawFrame1 /*, drawFrame2 */};
// UI overlays
OverlayCallback overlays[] = {updatesOverlay, ipOverlay, uptimeOverlay, freqOverlay};

void setupUi()
{
  // The ESP is capable of rendering 60fps in 80Mhz mode
  // but that won't give you much time for anything else
  // run it in 160Mhz mode or just set it to 30 fps
  ui.setTargetFPS(5);

  // Customize the active and inactive symbol
  //ui.setActiveSymbol(activeSymbol);
  //ui.setInactiveSymbol(inactiveSymbol);

  // You can change this to
  // TOP, LEFT, BOTTOM, RIGHT
  //ui.setIndicatorPosition(BOTTOM);

  // Defines where the first frame is located in the bar.
  //ui.setIndicatorDirection(LEFT_RIGHT);

  // You can change the transition that is used
  // SLIDE_LEFT, SLIDE_RIGHT, SLIDE_UP, SLIDE_DOWN
  //ui.setFrameAnimation(SLIDE_LEFT);

  // Add frames
  ui.setFrames(frames, 1);
  ui.disableAutoTransition();

  //ui.setTimePerFrame(3);

  // Add overlays
  ui.setOverlays(overlays, 4);

  // Initialising the UI will init the display too.
  ui.init();
}

//////////////////////////////////////////////////////////////////////////

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  display.init();
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_LEFT);

  WiFi.persistent(true);
  WiFi.begin();

  // Wait for connection
  int timeout = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(10);
    timeout++;

    // Waiting 5 seconds
    if (timeout > 500)
    {
      display.clear();
      display.drawString(0, 10, "WiFi disconnected");
      display.drawString(0, 26, "Check serial");
      display.display();

      Serial.println("Connection timeout.");
      Serial.setTimeout(15000);
      Serial.println("Enter WiFi network name:");
      String wifiSsid = Serial.readStringUntil('\n');

      if (wifiSsid.isEmpty())
      {
        continue;
      }

      Serial.println("Enter WiFi network password:");
      String wifiPass = Serial.readStringUntil('\n');

      Serial.println("Setting \"" + wifiSsid + "\" up");
      WiFi.begin(wifiSsid, wifiPass);

      timeout = 0;
    }
  }

  Serial.println("Connected!");
  Serial.println("IP: " + WiFi.localIP().toString());

  // Disablind display after emergency
  display.displayOff();
  display.end();
  // Enabling display back in UI mode
  setupUi();

  sendUpdateTicker.attach(TS_SEND_INTERVAL_SECS, []()
                          { itsTimeToSendUpdates = true; });
}

void loop()
{
  int remainingTimeBudget = ui.update();

  if (remainingTimeBudget > 0)
  {
    // You can do some work here
    // Don't do stuff if you are below your
    // time budget.
    t1Last = dsGetTemp(thermal1);
    t2Last = dsGetTemp(thermal2);

    Serial.println("T1 = " + (String)t1Last + "; T2 = " + (String)t2Last);

    if (itsTimeToSendUpdates)
    {
      float values[] = {t1Last, t2Last};
      sendUpdate(values, 2);
      updatesSent++;
      // Disabling until next tick
      itsTimeToSendUpdates = false;
    }

    delay(remainingTimeBudget);
  }
}
